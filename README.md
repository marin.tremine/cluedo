# Compte rendu SAE n°1
Dans ce compte rendu, nous allons voir comment installer une distribution linux 

## Méthode d'installation
Pour commencer, nous avons d'abord du réfléchir sur l'endroit ou nous allons installer notre distribution. Différentes solutions sont possibles : installation dual boot linux, machine virtuelle, wsl2, configuration des logiciels avec Windows, etc... Dans mon cas, j'ai choisi d'utiliser mon ordinateur portable avec un boot simple. 
## Choix de la distribution
Après avoir choisi son support d'installation, il faut maintenant réfléchir à quelle distribution linux on veut, il existe plus d'une centaine de distribution linux différente pour des besoins totalement divers. Chaque distribution à des avantages et des inconvénients, par exemple, certain sont légère, complète, facile à prendre en main, avec une joli interface graphique, avec une grosse communauté, dédié a des usages spécifiques etc... Pour choisir ma distribution j'ai utilisé le site [DistroWatch](https://distrowatch.com/), il répertorie toutes les distributions les plus utilisées dans le monde et établie un classement. On peut notamment avoir des informations directement sur les distributions.
## Installation de la distribution
Pour installer une distribution, il faut se rendre sur le site web de l'éditeur de la distribution et télécharger l'image iso de l'édition que l'on veut, c'est un fichier avec l'extension .iso. Ensuite avec cette image, il faut la graver sur une clé USB ou un disque, pour ce faire on peut utiliser des logiciels tels que [Etcher](https://www.balena.io/etcher/) ou [rufus](https://rufus.ie/fr/). Par contre, il faut attention car la clé USB sera formater, il faut donc faire attention à ne pas laisser de données importante dessus. Pour terminer il nous suffit juste de mettre la clé USB dans le pc ou l'on veut installer linux et de démarrer l'ordinateur depuis la clé USB. Cette manipulation ce fait dans le BIOS de l'ordinateur, pour y accéder il faut se renseigner auprès du constructeur de la carte mère. Il nous reste juste à suivre les instructions d'installation proposé par la distribution.

## Installation de logiciels et d'outils
Pour installer des applications sur linux, il existe différentes façons, certaines distribution proposent des applications déjà installer et/ou une logithèque mais principalement sur linux, on utilise un autre moyen qui est un gestionnaire de paquet (Sur linux, un logiciel est distribué sous la forme d'un paquet), c’est un outil qui permet d’installer des logiciels depuis des dépôts, de les désinstaller et de les mettre à jour. Chaque système d'exploitation à un gestionnaire de paquet installé de base.
Pour installer un paquet, il suffit juste d'écrire une commande suivi du nom du paquet. 

Par exemple, pour le gestionnaire de paquet Apt :

```bash
sudo apt install firefox vlc libreoffice
```

Quelquefois, le paquet que vous souhaitez installer ne se trouve pas dans les dépôts du système. Vous devrez donc ajouter un dépôt supplémentaire, celui dans lequel se trouve le paquet à installer.

## Mes choix
### Distribution linux 
Pour cette SAÉ, j'ai choisi la distribution linux mint c'est une distribution basée sur Ubuntu dont l'objectif est de fournir une expérience de bureau classique avec de nombreux outils pratiques et personnalisés et un support multimédia optionnel prêt à l'emploi. J'ai choisi cette distribution car je voulais un système d'exploitation de bureau élégant, facile à utiliser, à jour et confortable avec une forte communauté derrière. Je me suis donc rendu sur le site de [Linux Mint](https://www.linuxmint.com/) et j'ai choisi d'installer l'édition Cinnamon, c'est la plus complète et moderne mais aussi gourmande en termes de ressources. Connaissant la configuration de mon pc, ce n'était pas un problème, j'ai donc joint l'utile à l'agréable. 
### Logiciels
En accord avec les contraintes établies par le BUT et mon expérience, j'ai choisi d'utiliser certains logiciels tels que [Brave](https://brave.com/fr/) pour le navigateur internet, le client de messagerie [Thunderbird](https://www.thunderbird.net/fr/), les éditeurs de texte [sublimetext](https://www.sublimetext.com/) et [vscode](https://code.visualstudio.com/). [Obsidian](https://obsidian.md/) pour la prise de note, [Obs](https://obsproject.com/fr) pour l'enregistrement, le lecteur multimédia [Vlc](https://www.videolan.org/vlc/index.fr.html), [Teams](https://www.microsoft.com/fr-fr/microsoft-teams/group-chat-software) et [Discord](https://discord.com/) pour communiquer avec les professeurs, sans oublier la suite [office](https://www.office.com/). J'ai aussi installer [Rclone](https://rclone.org/), c'est un gestionnaire de fichier pour des clouds car il n'existe pas sous linux le logiciel google drive. Le tutoriel pour monter un cloud comme disque local avec [Rclone](https://rclone.org/) : https://doc.ubuntu-fr.org/tutoriel/monter_un_cloud.

### Scripts
Il y a deux script utilisable, le premier `install.sh` est pour installer les applications qui viennent d'être cité. Le deuxième `extension.sh` est pour installer les extensions que j'ai besoin sur Visual studio code.
Pour faire fonctionner les scripts, il faut les rendre exécutable si il ne le sont pas déjà avec la commande :
```bash
chmod u+x nom_du_fichier
```
puis les exécuter avec la commande :
```bash
./nom_du_fichier
```
